<?php

namespace Tests\Functional;

class ApiTest extends BaseTestCase
{
    /**
     * Test that the index route returns a rendered response containing the text 'RestApi' but not a Hello
     */
    public function testGetApiRoot()
    {
        $response = $this->runApp('GET', '/');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('RestApi', (string)$response->getBody());
        $this->assertNotContains('Hello', (string)$response->getBody());
    }

    /**
     * Test that the index route return the info facebook user by id
     */
    public function testGetApiInfoByIdCorrect()
    {
        $response = $this->runApp('GET', '/profile/facebook/571137275');

        $this->assertEquals(200, $response->getStatusCode());


        $data = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('name', $data);
    }


    /**
     * Test that the index route return error by facebook ID no exist
     */
    public function testGetApiWithError()
    {
        $response = $this->runApp('GET', '/profile/facebook/00000');

        $this->assertEquals(404, $response->getStatusCode());


        $data = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('error', $data);
    }

    /**
     * Test that the index route won't accept a post request
     */
    public function testPostApiNotAllowed()
    {
        $response = $this->runApp('POST', '/profile/facebook/571137275', ['test']);

        $this->assertEquals(405, $response->getStatusCode());
        $this->assertContains('Method not allowed', (string)$response->getBody());
    }
}
