<?php

use Slim\Http\Request;
use Slim\Http\Response;
use Facebook\Facebook;

$facebookAccess = array(
    'appId'     => '161332864263777',
    'appSecret' => '229efa4a25f52cfc5c57279a4f7d6783'
);

// Routes

$app->get('/', function (Request $request, Response $response) {
    $response->getBody()->write("RestApi Test");
    return $response;
});

$app->get('/profile/facebook/{fb_id}', function (Request $request, Response $response, array $args) use ($facebookAccess) {

    $id = $args['fb_id'];

    $fb = new Facebook(array(
        'app_id' => $facebookAccess['appId'],
        'app_secret' => $facebookAccess['appSecret'],
        'default_access_token' => $facebookAccess['appId'] .'|'. $facebookAccess['appSecret'],
        'default_graph_version' => 'v2.8'
    ));

    try {

        if ( ! (int) $id || $id < 0) {
            throw new Exception('Invalid parameter', 400);
        }

        $requestFB= $fb->get('/'.$id);

        $data = $requestFB->getDecodedBody();

        return $response->withJson(array(
                'id' => $data['id'],
                'name'=> $data['name']
            )
            , 200
        );
    }catch (Exception $e) {

        return $response->withJson(array(
                'error' => $e->getMessage()
            )
            , 404
        );
    }

});
