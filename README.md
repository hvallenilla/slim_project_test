# Slim Framework 3 | RestAPI Test

Esta aplicación es con fines de prueba y presentación

## 1. Clonar proyecto
    git clone git@bitbucket.org:hvallenilla/slim_project_test.git

## 2. Composer
    composer install

## 3. Ejecutar app
    php -S localhost:8080 -t public public/index.php
    
Ir al navegador o IDE para gestionar test de Api's y levantar

    http://localhost:8080/profile/facebook/571137275
    
"571137275" es un Facebook ID de prueba; use su ID y otros para testear la app

## Test
    ./vendor/bin/phpunit ./tests/Functional/ApiTest.php 
    


